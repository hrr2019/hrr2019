<?php

namespace app\controllers;

use app\models\Faculty;
use app\models\SpecialtyFacultyMap;
use Yii;
use app\models\Specialty;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SpecialtiesController implements the CRUD actions for Specialty model.
 */
class SpecialtiesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Specialty models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Specialty::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Specialty model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Specialty();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveFaculties($model->id, Yii::$app->request->post('hours'));
            return $this->redirect(['index']);
        }

        return $this->render('create', [
            'model' => $model,
            'facultiesMaps' => $this->getSpecialtyFacultyMaps(),
            'faculties' => $this->getFaculties(),
        ]);
    }

    /**
     * Updates an existing Specialty model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->saveFaculties($model->id, Yii::$app->request->post('hours'));
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'facultiesMaps' => $this->getSpecialtyFacultyMaps(),
            'faculties' => $this->getFaculties(),
        ]);
    }

    /**
     * Deletes an existing Specialty model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Specialty model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Specialty the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Specialty::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    private function getSpecialtyFacultyMaps()
    {
        return SpecialtyFacultyMap::find()->indexBy('faculty_id')->all();
    }

    private function getFaculties()
    {
        return Faculty::find()->all();
    }

    private function saveFaculties($specialtyId, $hours)
    {
        if (!$hours) {
            return;
        }

        SpecialtyFacultyMap::deleteAll();

        foreach ($hours as $faculty_id => $hour) {
            if (!$hours) {
                continue;
            }
            $map = new SpecialtyFacultyMap([
                'faculty_id' => $faculty_id,
                'specialty_id' => $specialtyId,
                'example_hour' => $hour,
            ]);
            $map->save();
        }


    }
}
