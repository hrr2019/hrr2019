<?php


namespace app\controllers;


use app\models\FacultyAnalytics;
use app\models\School;
use app\models\Specialty;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Controller;

class AnalyticsController extends Controller
{
    public function actionIndex()
    {
        $activeYear = Yii::$app->request->get('year');

        $needByYear = [
            2015 => 4406,
            2016 => 4642,
            2017 => 4851,
            2018 => 4862,
            2019 => 4855,
            2020 => 4878,
            2021 => 4911,
            2022 => 4944,
        ];

        $specialties = Specialty::find()->all();

        if (($specialtyId = Yii::$app->request->get('specialty')) && $activeYear) {
            $specialty = Specialty::findOne($specialtyId);

            $school = $this->getSchool();

            $facultyAnalyticsItems = [];
            foreach ($specialty->specialtyFacultyMap as $map) {
                $facultyAnalyticsItems[] = new FacultyAnalytics([
                    'facultyId' => $map->faculty_id,
                    'currentEmployeesHours' => round($specialty->student_count * $map->example_hour / $school->pupil_group_size),
                    'needEmployeesHours' => round($needByYear[$activeYear] * $map->example_hour / $school->pupil_group_size),
                ]);
            }

            $needByYear = array_map(function ($need) use ($specialty) {
                return round($need * $specialty->percent / 100);
            }, $needByYear);
        } else {
            $facultyAnalyticsItems = [];
        }

        $facultyAnalyticsProvider = new ArrayDataProvider([
            'allModels' => $facultyAnalyticsItems,
            'sort' => [
                'attributes' => [
                    'faculty.title',
                    'currentEmployeesHours',
                    'needEmployeesHours',
                ]
            ]
        ]);

        $chartData = [];
        foreach ($needByYear as $year => $need) {
            $chartData[] = [$need, $year];
        }

        return $this->render('index', [
            'specialties' => $specialties,
            'needByYear' => $needByYear,
            'urlParams' => Yii::$app->request->get(),
            'activeYear' => $activeYear,
            'facultyAnalyticsProvider' => $facultyAnalyticsProvider,
            'chartData' => $chartData,
        ]);
    }

    private function getSchool()
    {
        return School::findOne(1);
    }
}