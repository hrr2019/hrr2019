<?php


namespace app\controllers;


use app\models\School;
use Yii;
use yii\web\Controller;

class CommonConfController extends Controller
{
    public function actionIndex()
    {
        $school = $this->findSchool(1);

        if ($school->load(Yii::$app->request->post()) && $school->save()) {
            $this->redirect(['index']);
        }

        return $this->render('index', ['school' => $school]);
    }

    /**
     * @param $id
     * @return School|null
     */
    private function findSchool($id)
    {
        return School::findOne($id);
        // TODO Not found exception
    }
}