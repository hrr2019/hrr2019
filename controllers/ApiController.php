<?php

namespace app\controllers;

use app\models\EmployeesFaculty;
use app\models\Faculty;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class ApiController extends Controller
{
    public function actionEmploye()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->isPost) {//ajax response
            Yii::$app->response->format = Response::FORMAT_JSON;

            $empFa = new EmployeesFaculty();
            if ($_POST['employe_id']) {
                $empFa->count_hour = $_POST['count_hour'];
                $empFa->faculty_id = $_POST['faculty_id'];
                $empFa->employe_id = $_POST['employe_id'];
                $empFa->save();

                $facylty = Faculty::findOne(['id' => $_POST['faculty_id']]);

                return ['state' => true, 'faculty' => $facylty->title_ru];
            } else {
                return ['state' => false];
            }
        }
        return null;
    }
}
