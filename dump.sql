-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: z19145.adman.cloud    Database: hrr
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `middel_name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees`
--

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employees_faculty`
--

DROP TABLE IF EXISTS `employees_faculty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employees_faculty` (
  `faculty_id` int(11) DEFAULT NULL,
  `employe_id` int(11) DEFAULT NULL,
  `count_hour` int(11) DEFAULT NULL,
  KEY `employees_faculty_employees_id_fk` (`employe_id`),
  KEY `employees_faculty_faculty_id_fk` (`faculty_id`),
  CONSTRAINT `employees_faculty_employees_id_fk` FOREIGN KEY (`employe_id`) REFERENCES `employees` (`id`),
  CONSTRAINT `employees_faculty_faculty_id_fk` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employees_faculty`
--

LOCK TABLES `employees_faculty` WRITE;
/*!40000 ALTER TABLE `employees_faculty` DISABLE KEYS */;
/*!40000 ALTER TABLE `employees_faculty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `faculty`
--

DROP TABLE IF EXISTS `faculty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `faculty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `title_ru` varchar(250) DEFAULT NULL,
  `lesson_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `faculty`
--

LOCK TABLES `faculty` WRITE;
/*!40000 ALTER TABLE `faculty` DISABLE KEYS */;
INSERT INTO `faculty` VALUES (1,NULL,'IT-инфраструктура предприятия',NULL),(2,NULL,'Базы данных',NULL),(3,NULL,'Высокоуровневые методы информатики и программирования',NULL),(4,NULL,'Вычислительные системы, сети и телекоммуникации',NULL),(9,NULL,'Геотермальные процессы',NULL),(10,NULL,'Защита информации',NULL),(11,NULL,'Защита информации4',NULL),(12,NULL,'Имитационное моделирование экономических процессов',NULL),(13,NULL,'Интеллектуальные информационные системы',NULL),(14,NULL,'Интернет-программирование',NULL),(15,NULL,'Интернет-технологии ведения бизнеса',NULL),(16,NULL,'Информатика',NULL),(17,NULL,'Информатика и информационные технологии',NULL),(18,NULL,'Информатика и программирование',NULL),(19,NULL,'Информационная безопасность',NULL),(20,NULL,'Информационное обеспечение рыбоохранной деятельности',NULL),(21,NULL,'Информационные системы',NULL),(22,NULL,'Информационные системы в антикризисном управлении',NULL),(23,NULL,'Информационные системы в бухгалтерском учете',NULL),(24,NULL,'Информационные системы в промышленности',NULL),(25,NULL,'Информационные системы в рыбном хозяйстве',NULL),(26,NULL,'Информационные системы в экономике',NULL),(27,NULL,'Информационные системы и технологии',NULL),(28,NULL,'Информационные технологии',NULL),(29,NULL,'Информационные технологии в антикризисном управлении',NULL),(30,NULL,'Информационные технологии в бухгалтерском учете и аудите',NULL),(31,NULL,'Информационные технологии в менеджменте',NULL),(32,NULL,'Информационные технологии в управлении',NULL),(33,NULL,'Информационные технологии в управлении финансами',NULL),(34,NULL,'Информационные технологии в экономике',NULL),(35,NULL,'Информационные технологии управления',NULL),(36,NULL,'Информационный менеджмент',NULL),(37,NULL,'Компьютерные сети и интернет-технологии',NULL),(38,NULL,'Компьютерные технологии и статистические методы в экологии и природопользовании',NULL),(39,NULL,'Лабораторный практикум по компьютерному моделированию экономико-математических систем',NULL),(40,NULL,'Математическое и имитационное моделирование',NULL),(41,NULL,'Математическое обеспечение системы мониторинга рыболовства',NULL),(42,NULL,'Методики расчета экономической эффективности проектов',NULL),(43,NULL,'Методологии и технологии реинжиниринга и управления бизнес-процессами',NULL),(44,NULL,'Мировые информационные ресурсы',NULL),(45,NULL,'Моделирование геотермальных процессов',NULL),(46,NULL,'Моделирование экономических процессов и систем',NULL),(47,NULL,'Мультимедиа-технологии',NULL),(48,NULL,'Операционные системы,',NULL),(49,NULL,'Операционные системы, среды и оболочки',NULL),(50,NULL,'Основы алгоритмизации и языки программирования',NULL),(51,NULL,'Предметно-ориентированные экономические информационные системы',NULL),(52,NULL,'Программирование в среде СУБД',NULL),(53,NULL,'Программная инженерия',NULL),(54,NULL,'Проектирование информационных систем',NULL),(55,NULL,'Проектирование программного обеспечения информационных систем',NULL),(56,NULL,'Проектный практикум',NULL),(57,NULL,'Профессиональные компьютерные программы',NULL),(58,NULL,'Разработка и стандартизация программных средств и информационных технологий',NULL),(59,NULL,'Разработка программных приложений',NULL),(60,NULL,'Сетевая экономика',NULL),(61,NULL,'Теоретические основы создания информационного общества',NULL),(62,NULL,'Теория экономических информационных систем',NULL);
/*!40000 ALTER TABLE `faculty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `count` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_type`
--

DROP TABLE IF EXISTS `room_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_type`
--

LOCK TABLES `room_type` WRITE;
/*!40000 ALTER TABLE `room_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `room_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school`
--

DROP TABLE IF EXISTS `school`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `pupil_group_size` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school`
--

LOCK TABLES `school` WRITE;
/*!40000 ALTER TABLE `school` DISABLE KEYS */;
INSERT INTO `school` VALUES (1,'ЯрГУ им. П.Г.Демидова',NULL,25);
/*!40000 ALTER TABLE `school` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school_faculty_map`
--

DROP TABLE IF EXISTS `school_faculty_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_faculty_map` (
  `shool_id` int(11) DEFAULT NULL,
  `faculty_id` int(11) DEFAULT NULL,
  KEY `hschool_faculty_high_school_id_fk` (`shool_id`),
  KEY `hschool_faculty_faculty_id_fk` (`faculty_id`),
  CONSTRAINT `hschool_faculty_faculty_id_fk` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`),
  CONSTRAINT `hschool_faculty_high_school_id_fk` FOREIGN KEY (`shool_id`) REFERENCES `school` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school_faculty_map`
--

LOCK TABLES `school_faculty_map` WRITE;
/*!40000 ALTER TABLE `school_faculty_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `school_faculty_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `school_specialty_map`
--

DROP TABLE IF EXISTS `school_specialty_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `school_specialty_map` (
  `school_id` int(11) DEFAULT NULL,
  `specialty_id` int(11) DEFAULT NULL,
  KEY `hschool_specialty_high_school_id_fk` (`school_id`),
  KEY `hschool_specialty_specialty_id_fk` (`specialty_id`),
  CONSTRAINT `hschool_specialty_high_school_id_fk` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`),
  CONSTRAINT `hschool_specialty_specialty_id_fk` FOREIGN KEY (`specialty_id`) REFERENCES `specialty` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `school_specialty_map`
--

LOCK TABLES `school_specialty_map` WRITE;
/*!40000 ALTER TABLE `school_specialty_map` DISABLE KEYS */;
/*!40000 ALTER TABLE `school_specialty_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialty`
--

DROP TABLE IF EXISTS `specialty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specialty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `title_ru` varchar(250) DEFAULT NULL,
  `percent` int(11) DEFAULT NULL,
  `student_count` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialty`
--

LOCK TABLES `specialty` WRITE;
/*!40000 ALTER TABLE `specialty` DISABLE KEYS */;
INSERT INTO `specialty` VALUES (5,NULL,'Фронтенд-разработчик',15,720),(6,NULL,'PHP-разарботчик',12,NULL),(7,NULL,'Java-разработчик',12,NULL),(8,NULL,'Системный админитратор',10,NULL),(9,NULL,'Тестировщи',9,NULL),(10,NULL,'1С-программист',9,NULL),(11,NULL,'.NET-разработчик',9,NULL),(12,NULL,'C++-разарботчик',4,NULL),(13,NULL,'iOS-разарботчик',4,NULL),(14,NULL,'Android-разарботчик',4,NULL),(15,NULL,'Python-разраотчик',4,NULL),(16,NULL,'DevOps-инженер',3,NULL),(17,NULL,'JavaScript-разаработчик',2,NULL),(18,NULL,'Data scientist',2,NULL),(19,NULL,'Фулстек-раработчик',2,NULL);
/*!40000 ALTER TABLE `specialty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `specialty_faculty_map`
--

DROP TABLE IF EXISTS `specialty_faculty_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `specialty_faculty_map` (
  `specialty_id` int(11) DEFAULT NULL,
  `faculty_id` int(11) DEFAULT NULL,
  `lesson_hout` int(11) DEFAULT NULL,
  `example_hour` int(11) DEFAULT NULL,
  KEY `faculty_specialty_faculty_id_fk` (`faculty_id`),
  KEY `faculty_specialty_specialty_id_fk` (`specialty_id`),
  CONSTRAINT `faculty_specialty_faculty_id_fk` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`id`),
  CONSTRAINT `faculty_specialty_specialty_id_fk` FOREIGN KEY (`specialty_id`) REFERENCES `specialty` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `specialty_faculty_map`
--

LOCK TABLES `specialty_faculty_map` WRITE;
/*!40000 ALTER TABLE `specialty_faculty_map` DISABLE KEYS */;
INSERT INTO `specialty_faculty_map` VALUES (5,1,NULL,60),(5,2,NULL,45),(5,3,NULL,20),(5,4,NULL,30),(5,9,NULL,100),(5,10,NULL,120);
/*!40000 ALTER TABLE `specialty_faculty_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (1,'Hello world'),(2,'Go go go'),(3,'No no no');
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-07 16:43:38
