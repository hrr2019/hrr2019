<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "specialty_faculty_map".
 *
 * @property int $specialty_id
 * @property int $faculty_id
 * @property int $lesson_hout
 * @property int $example_hour
 *
 * @property Faculty $faculty
 * @property Specialty $specialty
 */
class SpecialtyFacultyMap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'specialty_faculty_map';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['specialty_id', 'faculty_id', 'lesson_hout', 'example_hour'], 'integer'],
            [['faculty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Faculty::className(), 'targetAttribute' => ['faculty_id' => 'id']],
            [['specialty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Specialty::className(), 'targetAttribute' => ['specialty_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'specialty_id' => 'Specialty ID',
            'faculty_id' => 'Faculty ID',
            'lesson_hout' => 'Lesson Hout',
            'example_hour' => 'Example Hour',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaculty()
    {
        return $this->hasOne(Faculty::className(), ['id' => 'faculty_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialty()
    {
        return $this->hasOne(Specialty::className(), ['id' => 'specialty_id']);
    }
}
