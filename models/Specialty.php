<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "specialty".
 *
 * @property int $id
 * @property string $name
 * @property string $title_ru
 *
 * @property string $title
 *
 * @property SchoolSpecialty[] $schoolSpecialties
 * @property SpecialtyFacultyMap[] $specialtyFacultyMap
 */
class Specialty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'specialty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'title_ru'], 'string', 'max' => 250],
            [['student_count'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title_ru' => 'Наименование',
            'student_count' => 'Количество студентов'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolSpecialtiesMap()
    {
        return $this->hasMany(SchoolSpecialtyMap::className(), ['specialty_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialtyFacultyMap()
    {
        return $this->hasMany(SpecialtyFacultyMap::className(), ['specialty_id' => 'id']);
    }

    public function getTitle()
    {
        return $this->title_ru;
    }
}
