<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "employees_faculty".
 *
 * @property int $faculty_id
 * @property int $employe_id
 * @property int $count_hour
 *
 * @property Employees $employe
 * @property Faculty $faculty
 */
class EmployeesFaculty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employees_faculty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['faculty_id', 'employe_id', 'count_hour'], 'integer'],
            [['employe_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employees::className(), 'targetAttribute' => ['employe_id' => 'id']],
            [['faculty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Faculty::className(), 'targetAttribute' => ['faculty_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'faculty_id' => 'Faculty ID',
            'employe_id' => 'Employe ID',
            'count_hour' => 'Count Hour',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmploye()
    {
        return $this->hasOne(Employees::className(), ['id' => 'employe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaculty()
    {
        return $this->hasOne(Faculty::className(), ['id' => 'faculty_id']);
    }
}
