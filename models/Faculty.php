<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faculty".
 *
 * @property int $id
 * @property string $name
 * @property string $title_ru
 * @property int $lesson_time
 *
 * @property string $title
 *
 * @property EmployeesFaculty[] $employeesFaculties
 * @property SchoolFacultyMap[] $schoolFacultyMaps
 * @property SpecialtyFacultyMap[] $specialtyFacultyMaps
 */
class Faculty extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'faculty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lesson_time'], 'integer'],
            [['name', 'title_ru'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title_ru' => 'Title Ru',
            'lesson_time' => 'Lesson Time',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeesFaculties()
    {
        return $this->hasMany(EmployeesFaculty::className(), ['faculty_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolFacultyMaps()
    {
        return $this->hasMany(SchoolFacultyMap::className(), ['faculty_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecialtyFacultyMaps()
    {
        return $this->hasMany(SpecialtyFacultyMap::className(), ['faculty_id' => 'id']);
    }

    public function getTitle()
    {
        return $this->title_ru;
    }
}
