<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "school".
 *
 * @property int $id
 * @property string $title
 * @property int $room_id
 * @property int $pupil_group_size
 *
 * @property SchoolFacultyMap[] $schoolFacultyMaps
 * @property SchoolSpecialtyMap[] $schoolSpecialtyMaps
 */
class School extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['room_id', 'pupil_group_size'], 'integer'],
            [['title'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'title' => 'Наименование',
            'pupil_group_size' => 'Размер группы студентов',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolFacultyMaps()
    {
        return $this->hasMany(SchoolFacultyMap::className(), ['shool_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolSpecialtyMaps()
    {
        return $this->hasMany(SchoolSpecialtyMap::className(), ['school_id' => 'id']);
    }
}
