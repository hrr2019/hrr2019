<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "school_faculty_map".
 *
 * @property int $shool_id
 * @property int $faculty_id
 *
 * @property Faculty $faculty
 * @property School $shool
 */
class SchoolFacultyMap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'school_faculty_map';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['shool_id', 'faculty_id'], 'integer'],
            [['faculty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Faculty::className(), 'targetAttribute' => ['faculty_id' => 'id']],
            [['shool_id'], 'exist', 'skipOnError' => true, 'targetClass' => School::className(), 'targetAttribute' => ['shool_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'shool_id' => 'Shool ID',
            'faculty_id' => 'Faculty ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaculty()
    {
        return $this->hasOne(Faculty::className(), ['id' => 'faculty_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShool()
    {
        return $this->hasOne(School::className(), ['id' => 'shool_id']);
    }
}
