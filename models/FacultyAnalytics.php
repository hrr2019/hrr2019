<?php


namespace app\models;


use yii\base\Model;

/**
 * @property Faculty $faculty
 */
class FacultyAnalytics extends Model
{
    public $facultyId;
    public $currentEmployeesHours;
    public $needEmployeesHours;

    /**
     * @return Faculty
     */
    public function getFaculty()
    {
        return Faculty::findOne(['id' => $this->facultyId]);
    }

    public function attributeLabels()
    {
        return [
            'faculty.title' => 'Факультет',
            'currentEmployeesHours' => 'Тек. кол-во часов',
            'needEmployeesHours' => 'Потребность в часах',
        ];
    }
}