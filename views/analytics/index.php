<?php
/**
 * @var \yii\web\View $view
 * @var Specialty[] $specialties
 * @var int[] $needByYear
 * @var int[] $chartData
 * @var array $urlParams
 * @var string|null $activeYear
 * @var \yii\data\BaseDataProvider $facultyAnalyticsProvider
 */

use app\models\Specialty;

?>

<div class="pull-left" style="width: 300px">
    <?= \yii\bootstrap\Nav::widget([
        'options' => [
            'class' => 'nav nav-pills nav-stacked'
        ],
        'items' => array_map(function (Specialty $specialty) use ($urlParams) {
            return [
                'label' => $specialty->getTitle(),
                'url' => ['analytics/index', 'specialty' => $specialty->id] + $urlParams,
            ];
        }, $specialties)
    ]) ?>
</div>

<div style="margin-left: 330px">
    <h4>Спрос на специалистов в области информационных технологий</h4>
    <div id="chart"></div>

    <div class="from-group">
        <?php foreach (array_keys($needByYear) as $year): ?>
            <?= \yii\helpers\Html::a($year, ['analytics/index', 'year' => $year] + $urlParams, [
                'class' => $activeYear == $year ? 'btn btn-primary' : 'btn btn-default',
            ]) ?>
        <?php endforeach; ?>
    </div>

    <?php
    if ($activeYear) {
        ?>
        <h4>Потребность в препадовательском составе</h4>

        <?= \yii\grid\GridView::widget([
            'dataProvider' => $facultyAnalyticsProvider,
            'columns' => [
                'faculty.title',
                'currentEmployeesHours',
                'needEmployeesHours',
            ]
        ]) ?>
        <?php
    } ?>
</div>

<script>
    window.chartData = <?= json_encode($chartData) ?>
</script>




