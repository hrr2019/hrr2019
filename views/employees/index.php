<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Employees';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employees-index">

    <ul class="nav nav-tabs">
        <li role="presentation"><?= Html::a('Основные', ['common-conf/index']) ?></li>
        <li role="presentation"><?= Html::a('Специальности', ['/specialties/index']) ?></li>
        <li role="presentation" class="active"><?= Html::a('Сотрудники', ['/employees/index']) ?></li>
    </ul>

    <br>

    <p>
        <?= Html::a('Добавить сотрудника', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'first_name',
            'last_name',
            'middel_name',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
