<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Employees */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employees-form">

    <?php $form = ActiveForm::begin(); ?>

    <div style="margin-left: 330px">
        <h4><?php
            if ($model->id) {
                echo $model->first_name . ' ' . $model->last_name . ' ' . $model->middel_name;
            }
            ?></h4>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'middel_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-12">
                <?php
                $facultyEmp = \app\models\EmployeesFaculty::findAll(['employe_id' => $model->id]);
                ?>
                <table class="table table-bordered" id="facultTable" data-index="<?= count($facultyEmp) ?>">
                    <tr>
                        <td>#</td>
                        <td>Факультет</td>
                        <td>Количество часов</td>
                        <td></td>
                    </tr>
                    <?php
                    foreach ($facultyEmp as $index => $faculty) {
                        ?>
                        <tr>
                            <td><?= $index ?></td>
                            <td><?= $faculty->faculty->title_ru ?></td>
                            <td><?= $faculty->count_hour ?></td>
                            <td><i class="glyphicon glyphicon-remove"
                                   id="<?= $faculty->faculty->id ?>"></i></td>
                        </tr>
                        <?php
                    }
                    ?>
                </table>
            </div>
            <div class="col-md-4">
                Факультет:
                <select class="form-control" id="selected">
                    <?php
                    $facultyes = \app\models\Faculty::find()->all();
                    foreach ($facultyes as $faculty) {
                        echo '<option value="' . $faculty->id . '">' . $faculty->title_ru . '</option>';
                    }
                    ?>
                </select>
            </div>
            <div class="col-md-4">
                Количество часов:
                <input type="number" class="form-control" id="countHour">
            </div>
            <div class="col-md-4">
                <label></label>
                <a class="btn btn-success addFaculty form-control" data-employe="<?= $model->id ?>">Добавить</a>
            </div>
            <div class="col-md-12" style="display: none">
                <div class="text-center
" style="background: red; padding: 7px">
                    <h4 style="color: white">Произошла ошибка!</h4>
                    <p style="color: white">Необходимо создать сотрудника!</p>
                </div>
            </div>
        </div>
    </div>


    <div class="form-group  text-right" style="margin-top: 15px">
        <?php
        $btnText = 'Добавить';
        if ($model->id) {
            $btnText = 'Сохранить';
        }
        ?>
        <?= Html::submitButton($btnText, ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
