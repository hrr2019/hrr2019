<?php

/* @var $this \yii\web\View */
/* @var $school \app\models\School */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<ul class="nav nav-tabs">
    <li role="presentation" class="active"><?= Html::a('Основные', ['common-conf/index']) ?></li>
    <li role="presentation"><?= Html::a('Специальности', ['/specialties/index']) ?></li>
</ul>

<br>

<div style="max-width: 400px">
    <?php $form = ActiveForm::begin() ?>

    <?= $form->field($school, 'title') ?>

    <?= $form->field($school, 'pupil_group_size') ?>

    <div class="form-group">
        <button class="btn btn-primary">Сохранить</button>
    </div>

    <?php ActiveForm::end() ?>
</div>

