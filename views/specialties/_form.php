<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Specialty */
/* @var $form yii\widgets\ActiveForm */
/* @var $facultiesMaps \app\models\SpecialtyFacultyMap[] */
/* @var $faculties \app\models\Specialty[] */
?>

<div class="specialty-form" style="max-width: 600px;">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-xs-8">
            <?= $form->field($model, 'title_ru')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-xs-4">
            <?= $form->field($model, 'student_count')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <table class="table table-bordered" id="table">
        <thead>
        <tr>
            <th>Предмет</th>
            <th class="td-hours">Кол-во часов</th>
            <!--            <th style="width: 50px"></th>-->
        </tr>
        </thead>

        <tbody>
        <tr id="empty-tr" class="<?= $facultiesMaps ? 'hidden' : '' ?>">
            <!--<td class="text-muted" colspan="3">Тут пока пусто</td>-->
        </tr>

        <?php foreach ($faculties as $faculty): ?>
            <tr>
                <td><?= Html::encode($faculty->title) ?></td>
                <td class="td-hours">
                    <input type="text" class="form-control house_info" data-id="<?= $faculty->id ?>"
                           value="<?= $facultiesMaps[$faculty->id]->example_hour ?? '' ?>"
                           name="hours[<?= $name??'' ?>]">
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <hr>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    /*function addFaculty() {
        var facultyId = $('#add-faculty-title').val();
        var hours = $('#add-hours').val();
        var $tr = $('<tr>');
        $('#empty-tr').addClass('hidden');
       //$('<td>').appendTo($tr).text($(`#add-faculty-title option[value="${facultyId}"]`).text());
        //$('<td class="td-hours">').appendTo($tr).html($(`<input name="hours[${facultyId}]" value="${hours}" class="form-control">`));
        // $('<td>').appendTo($tr).html(`<a href="#" class="glyphicon glyphicon-remove" onclick="delFaculty"></a>`);
        $('#table tbody').append($tr);
    }*/

    // function delFaculty(e) {
    //
    // }
</script>

<style>
    .td-hours {
        position: relative;
        width: 120px;
    }

    #table td {
        vertical-align: middle;
    }
</style>
