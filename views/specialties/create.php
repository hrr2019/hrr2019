<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Specialty */
/* @var $facultiesMaps \app\models\SpecialtyFacultyMap */
/* @var $faculties \app\models\Faculty[] */

$this->title = 'Создание специальности';
$this->params['breadcrumbs'][] = ['label' => 'Специальности', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specialty-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'facultiesMaps' => $facultiesMaps,
        'faculties' => $faculties,
    ]) ?>

</div>
