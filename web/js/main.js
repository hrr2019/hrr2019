function addedFaculty(e) {
    e.preventDefault();
    let idEmploye = this.dataset.employe;
    console.log(this);
    console.log(e);

    $.post({
        url: '/api/employe',
        dataType: 'json',
        data: {
            employe_id: idEmploye,
            faculty_id: $('#selected').val(),
            count_hour: $('#countHour').val()
        }
    }).done((r) => {
        if (r.state == true) {
            $('#error').hide();
            let facultTable = $('#facultTable');
            facultTable.append('<tr><td>' + facultTable.dataset.index + '</td><td>' + r.faculty + '</td><td>' + $('#countHour').val() + '</td><td><i class="glyphicon glyphicon-remove"></i></td></tr>');
            facultTable.dataset.index++;
        } else {
            $('#error').show();
        }
    });

}

$('.addFaculty').on('click', addedFaculty);

let chart = $('#chart');
if (chart) {
    pointArr = window.chartData;
    Highcharts.chart('chart', {
        chart: {
            type: 'spline',
            inverted: true
        },
        title: {
            text: '',
        },
        credits: {
            enabled: false
        },
        xAxis: {
            gridLineWidth: 1,
            reversed: false,
            title: {
                text: 'Уровень спроса на рынке IT специалистов'
            },
            tickInterval: 10
        },
        yAxis: {
            labels: {
                step: 0.5
            },
            title: {
                text: 'Год'
            },
            min: 2014,
            max: 2023,
            tickInternal: 10
        },
        legend: {
            enabled: false
        },
        tooltip: {
            enabled: true
        },
        plotOptions: {
            spline: {
                marker: {
                    enable: false
                }
            }
        },
        series: [{
            name: 'Получить занчение на этот год',
            data: pointArr,
            point: {
                events: {
                    click: function (e) {
                        let get = '?';
                        if (document.location.href.indexOf('?') !== -1) {
                            let data = document.location.href.split('?')[1];
                            data = data.split('&');
                            let startTrigger = true;
                            for (let i = 0; i < data.length; i++) {
                                if (data[i].indexOf('year') === -1) {
                                    if (startTrigger) {
                                        get = '?';
                                        startTrigger = false;
                                    }
                                    get += data[i];
                                }
                            }
                            if (startTrigger) {
                                get = '?';
                            } else {
                                get += '&'
                            }
                        }
                        document.location.href = get + 'year=' + e.point.options.y
                    }
                }
            }
        }]
    })
}